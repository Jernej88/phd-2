from SmartHousePy import SmartHouse, Battery_conf, Grid_conf
import numpy as np

def a_step(battery_max_charge,
           battery_max_discharge,
           battery_min_soc,
           battery_max_soc,
           battery_initial_soc,
           grid_max_to_grid,
           data_production,
           data_consumption,
           data_buy_price,
           data_sell_price,
           routine,
           new_battery_state,
           new_battery_flow,
           new_grid_flow,
           new_loss):
    test_battery = Battery_conf(battery_max_charge,
                                battery_max_discharge,
                                battery_min_soc,
                                battery_max_soc,
                                battery_initial_soc,
                                0.1, 1., 1.)
    test_grid = Grid_conf(grid_max_to_grid, 1.)

    test_data = np.empty((1,6))
    test_data[0,:] = np.array([30, data_consumption, data_production, -(data_production+data_consumption), data_buy_price, data_sell_price])
    
    sh = SmartHouse(test_data, test_battery, test_grid)
    sh.make_step(getattr(sh, routine))

    assert np.all(sh.get_state(1)[-4:] == np.array([new_battery_state,
                                                    new_battery_flow,
                                                    new_grid_flow,
                                                    new_loss]))
    return sh

def get_smarthouse_instance(nr_data_rows):
    test_battery = Battery_conf(
                            max_charge=1,
                            max_discharge=1,
                            min_soc=0,
                            max_soc=1,
                            initial_soc=0,
                            self_discharge_rate=0,
                            charge_efficiency=1,
                            discharge_efficiency=1
                            )
    test_grid = Grid_conf(max_to_grid=1, to_grid_efficiency=1)

    test_data = np.random.random((nr_data_rows, 6))
    test_data[:, 0] = np.arange(nr_data_rows) * 30
    test_data[:, 2] = - test_data[:, 2]
    test_data[:,3] = -(test_data[:, 2] + test_data[:,1])
    
    sh = SmartHouse(test_data, test_battery, test_grid)
    
    return sh


def partial_step(data_production,
                data_consumption,
                battery_initial_soc,
                routine,
                new_battery_state,
                new_battery_flow,
                new_grid_flow,
                new_loss):
                
    data_buy_price=0.1
    data_sell_price=0.01
    battery_max_charge=1
    battery_max_discharge=1
    battery_min_soc=0
    battery_max_soc=1
    grid_max_to_grid=1

    sh = a_step(battery_max_charge, battery_max_discharge, battery_min_soc, battery_max_soc, battery_initial_soc,
                grid_max_to_grid,
                data_production, data_consumption, data_buy_price, data_sell_price,
                routine,
                new_battery_state, new_battery_flow, new_grid_flow, new_loss)
    return sh

def compute_green(data_production, new_loss, new_grid_flow, data_consumption):
    if data_consumption == 0:
        return np.inf
    else:
        return (-data_production - new_loss - max(0, new_grid_flow)) / data_consumption


def test_use_grid_first():
    data_production=0
    data_consumption=0
    battery_initial_soc=0
    routine='use_grid_first'
    new_battery_state=0
    new_battery_flow=0
    new_grid_flow=0
    new_loss=0

    sh = partial_step(data_production=data_production,
                data_consumption=data_consumption,
                battery_initial_soc=battery_initial_soc,
                routine=routine,
                new_battery_state=new_battery_state,
                new_battery_flow=new_battery_flow,
                new_grid_flow=new_grid_flow,
                new_loss=new_loss)

    assert sh.cost() == 0
    assert sh.green() == compute_green(data_production, new_loss, new_grid_flow, data_consumption)

def test_use_grid_first_consume():
    data_production=0
    data_consumption=1
    battery_initial_soc=0
    routine='use_grid_first'
    new_battery_state=0
    new_battery_flow=0
    new_grid_flow=-1
    new_loss=0

    sh = partial_step(data_production=data_production,
                data_consumption=data_consumption,
                battery_initial_soc=battery_initial_soc,
                routine=routine,
                new_battery_state=new_battery_state,
                new_battery_flow=new_battery_flow,
                new_grid_flow=new_grid_flow,
                new_loss=new_loss)

    assert sh.cost() == 0.1
    assert sh.green() == compute_green(data_production, new_loss, new_grid_flow, data_consumption)


def test_use_grid_first_produce():
    data_production=-1
    data_consumption=0
    battery_initial_soc=0
    routine='use_grid_first'
    new_battery_state=0
    new_battery_flow=0
    new_grid_flow=1
    new_loss=0

    sh = partial_step(data_production=data_production,
                data_consumption=data_consumption,
                battery_initial_soc=battery_initial_soc,
                routine=routine,
                new_battery_state=new_battery_state,
                new_battery_flow=new_battery_flow,
                new_grid_flow=new_grid_flow,
                new_loss=new_loss)

    assert sh.cost() == -0.01
    assert sh.green() == compute_green(data_production, new_loss, new_grid_flow, data_consumption)


def test_use_battery_first():
    data_production=0
    data_consumption=0
    battery_initial_soc=0
    routine='use_battery_first'
    new_battery_state=0
    new_battery_flow=0
    new_grid_flow=0
    new_loss=0

    sh = partial_step(data_production=data_production,
                data_consumption=data_consumption,
                battery_initial_soc=battery_initial_soc,
                routine=routine,
                new_battery_state=new_battery_state,
                new_battery_flow=new_battery_flow,
                new_grid_flow=new_grid_flow,
                new_loss=new_loss)

    assert sh.cost() == 0.
    assert sh.green() == compute_green(data_production, new_loss, new_grid_flow, data_consumption)


def test_use_battery_first_consume_empty_battery():
    data_production=0
    data_consumption=1
    battery_initial_soc=0
    routine='use_battery_first'
    new_battery_state=0
    new_battery_flow=0
    new_grid_flow=-1
    new_loss=0

    sh = partial_step(data_production=data_production,
                data_consumption=data_consumption,
                battery_initial_soc=battery_initial_soc,
                routine=routine,
                new_battery_state=new_battery_state,
                new_battery_flow=new_battery_flow,
                new_grid_flow=new_grid_flow,
                new_loss=new_loss)

    assert sh.cost() == 0.1
    assert sh.green() == compute_green(data_production, new_loss, new_grid_flow, data_consumption)


def test_use_battery_first_consume_full_battery():
    data_production=0
    data_consumption=1
    battery_initial_soc=1
    routine='use_battery_first'
    new_battery_state=0
    new_battery_flow=-1
    new_grid_flow=0
    new_loss=0
                
    sh = partial_step(data_production=data_production,
                data_consumption=data_consumption,
                battery_initial_soc=battery_initial_soc,
                routine=routine,
                new_battery_state=new_battery_state,
                new_battery_flow=new_battery_flow,
                new_grid_flow=new_grid_flow,
                new_loss=new_loss)

    assert sh.cost() == 0.
    assert sh.green() == compute_green(data_production, new_loss, new_grid_flow, data_consumption)


def test_use_battery_first_produce():
    data_production=-1
    data_consumption=0
    battery_initial_soc=0
    routine='use_battery_first'
    new_battery_state=1
    new_battery_flow=1
    new_grid_flow=0
    new_loss=0
                
    sh = partial_step(data_production=data_production,
                data_consumption=data_consumption,
                battery_initial_soc=battery_initial_soc,
                routine=routine,
                new_battery_state=new_battery_state,
                new_battery_flow=new_battery_flow,
                new_grid_flow=new_grid_flow,
                new_loss=new_loss)

    assert sh.cost() == 0.0
    assert sh.green() == compute_green(data_production, new_loss, new_grid_flow, data_consumption)



def test_use_battery_first_produce_also_to_grid():
    data_production=-2
    data_consumption=0
    battery_initial_soc=0
    routine='use_battery_first'
    new_battery_state=1
    new_battery_flow=1
    new_grid_flow=1
    new_loss=0
                
    sh = partial_step(data_production=data_production,
                data_consumption=data_consumption,
                battery_initial_soc=battery_initial_soc,
                routine=routine,
                new_battery_state=new_battery_state,
                new_battery_flow=new_battery_flow,
                new_grid_flow=new_grid_flow,
                new_loss=new_loss)

    assert sh.cost() == -0.01
    assert sh.green() == compute_green(data_production, new_loss, new_grid_flow, data_consumption)


def test_use_battery_first_produce_also_to_grid_loss():
    data_production=-3
    data_consumption=0
    battery_initial_soc=0
    routine='use_battery_first'
    new_battery_state=1
    new_battery_flow=1
    new_grid_flow=1
    new_loss=1
                
    sh = partial_step(data_production=data_production,
                data_consumption=data_consumption,
                battery_initial_soc=battery_initial_soc,
                routine=routine,
                new_battery_state=new_battery_state,
                new_battery_flow=new_battery_flow,
                new_grid_flow=new_grid_flow,
                new_loss=new_loss)

    assert sh.cost() == -0.01
    assert sh.green() == compute_green(data_production, new_loss, new_grid_flow, data_consumption)


def test_composed_one_level():
    sh = get_smarthouse_instance(1)
    sh.make_step(sh.ith_leq_then_else(0, 60, sh.use_battery_first, sh.use_grid_first))