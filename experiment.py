"""Specifies the experiment."""

# get data -> train + test
# define simulator
# define optimizator
# run optimization - logging everything
# run test
# save results
import os
from shutil import copyfile, copy, copytree
from datetime import datetime
import logging
import json
import subprocess

from optimizers import optUtils

logger = logging.getLogger('experiment')
logger.setLevel(logging.DEBUG)

# create file handler which logs even debug messages
fh = logging.FileHandler('experiment.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s name=%(name)s level=%(levelname)s %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)


from data import load_data
import data
#from SmartHouse import get_test_smart_house
import SmartHouse
import optimizers.GP
import optimizers.NSGAII
import utils

import optSettings

class Experiment(object):
    
    def __init__(self):
        
        # saving the data files
        os.makedirs('logs/', exist_ok=True)
        
        EXPERIMENT_ID = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
        EXPERIMENT_LOG_DIR = "logs/" + EXPERIMENT_ID
        
        os.makedirs(EXPERIMENT_LOG_DIR)
        self.experiment_log_dir = EXPERIMENT_LOG_DIR
        self.logger = logging.getLogger('Experiment')
        self.logger.setLevel(logging.DEBUG)

        fh2 = logging.FileHandler(EXPERIMENT_LOG_DIR + '/log')
        fh2.setLevel(logging.DEBUG)
        fh2.setFormatter(formatter)
        self.logger.addHandler(fh2)
   
        self.logger.debug('msg="saving state" status=STARTED')
        os.makedirs(EXPERIMENT_LOG_DIR + '/data/')
        for folder in ["consumption", "prices", "weather"]:
            os.makedirs(EXPERIMENT_LOG_DIR + '/data/processed/' + folder + '/')
        copyfile('data.py', EXPERIMENT_LOG_DIR + '/data.py')
        copyfile('experiment.py', EXPERIMENT_LOG_DIR + '/experiment.py')    
        copyfile('optSettings.py', EXPERIMENT_LOG_DIR + '/optSettings.py')
        copyfile('SmartHouse.py', EXPERIMENT_LOG_DIR + '/SmartHouse.py')
        copyfile('utils.py', EXPERIMENT_LOG_DIR + '/utils.py')
        copytree('optimizers/', EXPERIMENT_LOG_DIR + '/optimizers/')
        utils.write_env(EXPERIMENT_LOG_DIR + '/requirements.txt')
        utils.write_runtime(EXPERIMENT_LOG_DIR + '/runtime.txt')
        utils.write_runtime_full(EXPERIMENT_LOG_DIR + '/runtime-full.txt')
        utils.copy_load(optSettings.DATA_LOAD_PATH, optSettings.DATA_LOAD_ID, EXPERIMENT_LOG_DIR + '/' + optSettings.DATA_LOAD_PATH)
        copyfile(optSettings.DATA_PRICES_PATH, EXPERIMENT_LOG_DIR + '/' + optSettings.DATA_PRICES_PATH)
        copyfile(optSettings.DATA_WEATHER_PATH, EXPERIMENT_LOG_DIR + '/' + optSettings.DATA_WEATHER_PATH)
        
        with open(self.experiment_log_dir + '/opt-mode.txt', 'w') as ff:
            ff.write(optSettings.OPT_METHOD)

        self.logger.debug('msg="saving state" status=FINISHED')

        self.logger.debug('msg="initializing experiment instances", status=STARTED')
        BATTERY_CONF = SmartHouse.Battery_conf(max_charge=optSettings.BATTERY_MAX_CHARGE,
                                               max_discharge=optSettings.BATTERY_MAX_DISCHARGE,
                                               min_soc=optSettings.BATTERY_MIN_SOC,
                                               max_soc=optSettings.BATTERY_MAX_SOC,
                                               initial_soc=optSettings.BATTERY_INITIAL_SOC,
                                               self_discharge_rate=optSettings.BATTERY_SELF_DISCHARGE_RATE,
                                               charge_efficiency=optSettings.BATTERY_CHARGE_EFFICIENCY,
                                               discharge_efficiency=optSettings.BATTERY_DISCHARGE_EFFICIENCY)

        GRID_CONF = SmartHouse.Grid_conf(max_to_grid=optSettings.GRID_MAX_TO_GRID,
                                         to_grid_efficiency=optSettings.GRID_TO_GRID_EFFICIENCY)

        TRAIN_DATA, TEST_DATA = data.load_train_test_data(train_start=optSettings.DATA_TRAIN_START,
                                                          train_end=optSettings.DATA_TRAIN_END,
                                                          test_start=optSettings.DATA_TEST_START,
                                                          test_end=optSettings.DATA_TEST_END,
                                                          load_path=optSettings.DATA_LOAD_PATH,
                                                          load_id=optSettings.DATA_LOAD_ID,
                                                          prices_path=optSettings.DATA_PRICES_PATH,
                                                          prices_buy_id=optSettings.DATA_PRICES_BUY_ID,
                                                          prices_sell_id=optSettings.DATA_PRICES_SELL_ID,
                                                          weather_path=optSettings.DATA_WEATHER_PATH,
                                                          weather_max_power=optSettings.PV_PEAK_POWER)

        self.TRAIN_HOUSE = SmartHouse.SmartHouse(TRAIN_DATA, BATTERY_CONF, GRID_CONF, features=None, debug=True)

        self.TEST_HOUSE = SmartHouse.SmartHouse(TEST_DATA, BATTERY_CONF, GRID_CONF, features=None, debug=True)
        
        if optSettings.OPT_METHOD == "GP":
            self.optimization = optimizers.GP.GPOptimizer(smart_house=self.TRAIN_HOUSE,
                                                     NMU=optSettings.OPT_GP_NMU,
                                                     NLAMBDA=optSettings.OPT_GP_NLAMBDA,
                                                     NGEN=optSettings.OPT_GP_NGEN,
                                                     MUTPB=optSettings.OPT_GP_MUTPB,
                                                     CXPB=optSettings.OPT_GP_CXPB,
                                                     INIT_MIN_DEPTH=optSettings.OPT_GP_INIT_MIN_DEPTH,
                                                     INIT_MAX_DEPTH=optSettings.OPT_GP_INIT_MAX_DEPTH,
                                                     MUT_MIN_DEPTH=optSettings.OPT_GP_MUT_MIN_DEPTH,
                                                     MUT_MAX_DEPTH=optSettings.OPT_GP_MUT_MAX_DEPTH,
                                                     MAX_DEPTH=optSettings.OPT_GP_MAX_DEPTH,
                                                     logdir=EXPERIMENT_LOG_DIR)
        elif optSettings.OPT_METHOD == "NSGAII":
            self.optimization = optimizers.NSGAII.GAOptimizer(smart_house=self.TRAIN_HOUSE,
                                                     NMU=optSettings.OPT_GA_NMU,
                                                     NLAMBDA=optSettings.OPT_GA_NLAMBDA,
                                                     NGEN=optSettings.OPT_GA_NGEN,
                                                     MUTPB=optSettings.OPT_GA_MUTPB,
                                                     CXPB=optSettings.OPT_GA_CXPB,
                                                     logdir=EXPERIMENT_LOG_DIR,
                                                     optimal=optSettings.OPT_GA_OPTIMAL)
        else:
            raise f"OPT_METHOD: {optSettings.OPT_METHOD} not defined!"

        self.logger.debug('msg="initializing experiment instances" status=FINISHED')
    
    def export_tree(self, routine, smart_house_name):
        str_repr = routine.__str__()
        str_repr = str_repr.replace('if_', smart_house_name + '.if_')
        return eval(str_repr)
    
    def compress_file(self, route):
        self.logger.debug('msg="compressing" arg={} status=STARTED'.format(route))
        output = subprocess.run(['tar', '-cvzf', '{}.tar.gz'.format(route), route])
        try:
            output.check_returncode()
        except subprocess.CalledProcessError:
            self.logger.warn(output.stderr)
            
        self.logger.debug('msg=compressing" arg={} status=FINISHED'.format(route))

    def run(self):
        self.logger.debug('msg="optimization run" status=STARTED')
        population, statistics = self.optimization.optimize()
        
        self.logger.debug('msg="optimization run" status=FINISHED')
        
        big_file_path = self.experiment_log_dir + '/logIndividualsPopulation.db'
        self.compress_file(big_file_path)
        os.unlink(big_file_path)
        
        big_file_path = self.experiment_log_dir + '/logIndividualsOffspring.db'
        self.compress_file(big_file_path)
        os.unlink(big_file_path)
        
        return population, statistics
    
    
    def save_properties(self, properties, ids, column_name, filename):
        with open(self.experiment_log_dir + '/' + filename, 'a') as fp:
            fp.write(f"individual_id;{column_name}\n")
            for prop, ind_id in zip(properties, ids):
                fp.write(f"{ind_id};{prop}\n")
        
        
    def test(self, population):
        self.logger.debug('msg="testing" status=STARTED')
        ids = [ind.uuid for ind in population]
        train_fits = []
        train_max_peaks = []
        train_battery_context_switches = []
        train_sar_traces = []
        test_fits = []
        test_max_peaks = []
        test_battery_context_switches = []
        test_sar_traces = []
        
        self.TEST_HOUSE.SAR_tracking = True
        self.TRAIN_HOUSE.SAR_tracking = True
        
        if optSettings.OPT_METHOD == "NSGAII":
            evaluate = optimizers.NSGAII.evaluate_timetable
        elif optSetting.OPT_METHOD == "GP":
            evaluate = optimizers.GP.evaluate_routine
            
        for ind in population:
            if optSettings.OPT_METHOD == "GP":
                self.TRAIN_HOUSE.features_dynamic.update(self.TRAIN_HOUSE.features_static)
                self.TRAIN_HOUSE.routine_attr.update(ind.routine_attr)
                self.TRAIN_HOUSE.run(self.export_tree(ind, "self.TRAIN_HOUSE"), reset_attributes=False)
            elif optSettings.OPT_METHOD == "NSGAII":
                self.TRAIN_HOUSE.run_timetable(ind)
            train_fits.append(self.TRAIN_HOUSE.fitness)
            train_max_peaks.append(self.TRAIN_HOUSE.max_peak)
            train_battery_context_switches.append(self.TRAIN_HOUSE.battery_context_switches)
            train_sar_traces.append(self.TRAIN_HOUSE.state_action_reward_trace)
        
        for ind in population:
            if optSettings.OPT_METHOD == "GP":
                self.TEST_HOUSE.features_static.update(self.TRAIN_HOUSE.features_static)
                self.TEST_HOUSE.features_dynamic.update(self.TRAIN_HOUSE.features_static)
                self.TEST_HOUSE.routine_attr.update(ind.routine_attr)
                self.TEST_HOUSE.run(self.export_tree(ind, "self.TEST_HOUSE"), reset_attributes=False)
            elif optSettings.OPT_METHOD == "NSGAII":
                self.TEST_HOUSE.run_timetable(ind)
            test_fits.append(self.TEST_HOUSE.fitness)
            test_max_peaks.append(self.TEST_HOUSE.max_peak)
            test_battery_context_switches.append(self.TEST_HOUSE.battery_context_switches)
            test_sar_traces.append(self.TEST_HOUSE.state_action_reward_trace)
        
        
        self.save_properties(train_fits, ids, 'fitness', 'train_fits.csv')
        self.save_properties(test_fits, ids, 'fitness', 'test_fits.csv')
        self.save_properties(train_max_peaks, ids, 'max_peak', 'train_max_peaks.csv')
        self.save_properties(test_max_peaks, ids, 'max_peak', 'test_max_peaks.csv')
        self.save_properties(train_battery_context_switches,
                             ids, 'battery_context_switches',
                             'train_battery_context_switches.csv')
        self.save_properties(test_battery_context_switches,
                             ids, 'battery_context_switches',
                             'test_battery_context_switches.csv')
        self.save_properties(train_sar_traces,
                             ids, 'sar_traces',
                             'train_sar_traces.csv')
        self.save_properties(test_sar_traces,
                             ids, 'sar_traces',
                             'test_sar_traces.csv')
        
        
        big_file_path = self.experiment_log_dir + '/train_sar_traces.csv'
        self.compress_file(big_file_path)
        os.unlink(big_file_path)
       
        big_file_path = self.experiment_log_dir + '/test_sar_traces.csv'
        self.compress_file(big_file_path)
        os.unlink(big_file_path)
        
        self.logger.debug('msg="testing" status=FINISHED')
        return train_fits, test_fits, train_max_peaks, test_max_peaks
    
    def run_predefined(self):
        self.logger.debug('msg="run_predefined" status=STARTED')
        results = {}
        
        # use grid first
        self.TEST_HOUSE.run(self.TEST_HOUSE.use_grid_first)
        fits = self.TEST_HOUSE.fitness
        max_peak = self.TEST_HOUSE.max_peak
        battery_context_switches = self.TEST_HOUSE.battery_context_switches
        results["use_grid_first"] = {}
        results["use_grid_first"]["test"] = {
            "fitnes": fits,
            "max_peak": max_peak,
            "battery_context_switches": battery_context_switches,
        }
        
        self.TRAIN_HOUSE.run(self.TRAIN_HOUSE.use_grid_first)
        fits = self.TRAIN_HOUSE.fitness
        max_peak = self.TRAIN_HOUSE.max_peak
        battery_context_switches = self.TRAIN_HOUSE.battery_context_switches
        results["use_grid_first"]["train"] = {
            "fitnes": fits,
            "max_peak": max_peak,
            "battery_context_switches": battery_context_switches,
        }
        
        # use battery first
        self.TEST_HOUSE.run(self.TEST_HOUSE.use_battery_first)
        fits = self.TEST_HOUSE.fitness
        max_peak = self.TEST_HOUSE.max_peak
        battery_context_switches = self.TEST_HOUSE.battery_context_switches
        results["use_battery_first"] = {}
        results["use_battery_first"]["test"] = {
            "fitnes": fits,
            "max_peak": max_peak,
            "battery_context_switches": battery_context_switches,
        }
        
        self.TRAIN_HOUSE.run(self.TRAIN_HOUSE.use_battery_first)
        fits = self.TRAIN_HOUSE.fitness
        max_peak = self.TRAIN_HOUSE.max_peak
        battery_context_switches = self.TRAIN_HOUSE.battery_context_switches
        results["use_battery_first"]["train"] = {
            "fitnes": fits,
            "max_peak": max_peak,
            "battery_context_switches": battery_context_switches,
        }  
        
        # use opportunist sell
        self.TEST_HOUSE.run(self.TEST_HOUSE.use_oportunist_sell)
        fits = self.TEST_HOUSE.fitness
        max_peak = self.TEST_HOUSE.max_peak
        battery_context_switches = self.TEST_HOUSE.battery_context_switches
        results["use_oportunist_sell"] = {}
        results["use_oportunist_sell"]["test"] = {
            "fitnes": fits,
            "max_peak": max_peak,
            "battery_context_switches": battery_context_switches,
        }
        
        self.TRAIN_HOUSE.run(self.TRAIN_HOUSE.use_oportunist_sell)
        fits = self.TRAIN_HOUSE.fitness
        max_peak = self.TRAIN_HOUSE.max_peak
        battery_context_switches = self.TRAIN_HOUSE.battery_context_switches
        results["use_oportunist_sell"]["train"] = {
            "fitnes": fits,
            "max_peak": max_peak,
            "battery_context_switches": battery_context_switches,
        }  
        
        self.logger.debug('msg="run_predefined" status=FINISHED')
        
        with open(self.experiment_log_dir + '/predefined_routines_results.json', 'w') as ff:
            json.dump(results, ff)            
        
        return results
    

if __name__ == "__main__":
    import numpy as np
    
    exp = Experiment()
    
    mode = "run_experiment"
    #mode = "run_predefined"
    #mode = "run_test"
    
    if mode == "run_experiment":
        logger.debug('msg="Entering the Experiment" status=STARTED')
        pop, stat = exp.run()
        logger.debug('msg="Entering the Experiment" status=FINISHED')      

        train_fits, test_fits, *_ = exp.test(pop)

        diffs = [(ftr[0] - fte[0], ftr[1] - fte[1]) for ftr, fte in zip(train_fits, test_fits)]

        diffs_cost, diffs_green = zip(*diffs)

        print(f"Cost diffs: {np.mean(diffs_cost)} (+/- {np.std(diffs_cost)})")
        print(f"Green diffs: {np.mean(diffs_green)} (+/- {np.std(diffs_green)})")
        
        logger.debug('msg="Entering the run predefined" status=STARTED')
        result = exp.run_predefined()
        print(json.dumps(result))
        logger.debug('msg="Entering the run predefined" status=FINISHED')
        
    elif mode == "run_predefined":
        logger.debug('msg="Entering the run predefined" status=STARTED')
        result = exp.run_predefined()
        print(json.dumps(result))
        logger.debug('msg="Entering the run predefined" status=FINISHED')
        
        
    elif mode == "run_test":
        logger.debug('msg="Entering the run test" status=STARTED')
        
        logger.debug('msg="Entering the run test" status=FINISHED')
    
    