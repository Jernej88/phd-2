import numpy as np
from functools import partial
from random import random
import pandas as pd
import numpy as np
import pandas as pd
import json
from collections import namedtuple
from functools import partial
import os
from datetime import datetime
from copy import deepcopy
from itertools import cycle

import matplotlib.pyplot as plt

# # SIMULATOR
#
#1. Simulacija dobi samo podatke, že vse očiščene etc.
#2. Vsi podatki so v istem formatu - **kWh** za energijo (kW ni!!!)
#3. Če imamo objekt, ki vsebuje samo podatke, uporabimo named_tuple
#4. Naprava ima pozitiven flow, če vanjo teče energija ter negativnega, če iz nje energija gre
#5. Kar ima za posledico, da je `data_pv` vedno negativna, `data_load` vedno pozitivna, `flow_loss` vedno pozitivna
#6. Za začetek ne bo izgub v sistemu
#7. Kar dobimo kot vhodni podatek je **data**, kar računamo je **flow** ali **state**
#8. Balance je `data_balance = - (data_pv + data_load)`, da se ujema z zdravo pametjo

Battery_conf = namedtuple('Battery_conf', 'max_charge max_discharge min_soc max_soc initial_soc self_discharge_rate charge_efficiency discharge_efficiency')

Data_in = namedtuple('Data_in', 'data_time data_load data_pv data_buy_prices data_sell_prices')

Grid_conf = namedtuple('Grid_conf', 'max_to_grid to_grid_efficiency')

def validate_Data_in(data_in):
    assert data_in.data_time.shape[0] == data_in.data_load.shape[0]
    assert data_in.data_load.shape[0] == data_in.data_pv.shape[0]
    assert data_in.data_pv.shape[0] == data_in.data_buy_prices.shape[0]
    assert data_in.data_buy_prices.shape[0] == data_in.data_sell_prices.shape[0]

def if_then_else(condition, out1, out2):
    out1() if condition() else out2()
    
def add_features(data_in, features):
    for n,v in data_in._asdict().items():
        features[n + '_min'] = min(v)
        features[n + '_max'] = max(v)

class SmartHouse(object):
    
    __slots__ = ('features_static',
                'features_dynamic',
                'data_time',
                'data_load',
                'data_pv',
                'data_buy_prices',
                'data_sell_prices',
                'settings_battery',
                'settings_grid',
                'data_length',
                'data_balance',
                'debug',
                'keep_attrs',
                'minute_of_day',
                'pday_avg_data_buy_prices',
                'pday_avg_data_sell_prices',
                'diff_pday_avg_data_sell_prices',
                'diff_pday_avg_data_buy_prices',
                'old_fitness',
                'flow_battery',
                'state_battery',
                'flow_grid',
                'flow_loss',
                'now',
                'now_state_battery',
                'now_balance',
                'routine_attr',
                'routine_attr_id',
                'routine_attr_bool',
                'new_flow_grid',
                'new_flow_battery',
                'primitives',
                'terminals',
                'state_action_reward_trace',
                'action_executed',
                'SAR_tracking')
    
    
    def __init__(self, data_in, settings_battery, settings_grid,
                 features=None, SAR_tracking=False, debug=False):
        self.features_static = dict()
        
        for n,v in data_in._asdict().items():
            if n != 'data_time':
                self.features_static[n + '_min'] = min(v)
                self.features_static[n + '_max'] = max(v)

        if features is not None:
            self.features_static.update(features)
        
        #add_features(data_in, self.features_static)
        #self.features_static = list(self.features_static.items())
        self.features_dynamic = dict()
        self.features_dynamic.update(self.features_static)
        
        
        self.data_time = data_in.data_time
        self.data_load = data_in.data_load
        self.data_pv = data_in.data_pv
        self.data_buy_prices = data_in.data_buy_prices
        self.data_sell_prices = data_in.data_sell_prices
        
        self.settings_battery = settings_battery
        self.settings_grid = settings_grid
        self.data_length = data_in.data_time.shape[0]
        self.data_balance = - (data_in.data_pv + data_in.data_load)
        
        self.debug = debug
        #self.keep_attrs = False
        self._reset()
        
        #self.register_minute_of_day()
        get_minutes = lambda x: x.hour * 60 + x.minute
        self.minute_of_day = np.zeros(self.data_length)
        for i in range(self.data_length):
            self.minute_of_day[i] = get_minutes(self.data_time[i])
        self._features_update('minute_of_day')
        
        #register previous day average buy/sell
        df_ = pd.DataFrame({'time':self.data_time,'buy':self.data_buy_prices, 'sell':self.data_sell_prices})
        avgs = df_.groupby([x.date() for x in df_.time]).mean()
        avgs = avgs.shift(1).fillna(method='bfill')
        self.pday_avg_data_buy_prices = df_.time.map(lambda x: avgs.loc[x.date(),'buy']).values
        self._features_update('pday_avg_data_buy_prices')
        self.pday_avg_data_sell_prices = df_.time.map(lambda x: avgs.loc[x.date(),'sell']).values
        self._features_update('pday_avg_data_sell_prices')
        
        #register difference to previous day average buy/sell
        self.diff_pday_avg_data_buy_prices = self.data_buy_prices - self.pday_avg_data_buy_prices 
        self._features_update('diff_pday_avg_data_buy_prices')
        
        self.diff_pday_avg_data_sell_prices = self.data_sell_prices - self.pday_avg_data_sell_prices 
        self._features_update('diff_pday_avg_data_sell_prices')        
        
        self.old_fitness = 0, 0
        
        # states of the world
        # data_time.dayofweek, data_time.month, data_time.hour, minute_of_day
        # data_load, data_pv, data_buy_prices, data_sell_prices
        # pday_avg_data_buy_prices, pday_avg_data_sell_prices
        # diff_pday_avg_data_buy_prices, diff_pday_avg_data_sell_prices
        self.state_action_reward_trace = []
        self.action_executed = None
        
        # do we want to store SAR trace?
        self.SAR_tracking = SAR_tracking
        
    def _features_update(self, feature_name):
        self.features_static[feature_name + '_min'] = min(getattr(self, feature_name))
        self.features_static[feature_name + '_max'] = max(getattr(self, feature_name))
        self.features_dynamic.update(self.features_static)
   
    #########################################################
    def _reset(self, reset_attributes=True):
        self.flow_battery = np.zeros(self.data_length)
        self.state_battery = np.zeros(self.data_length)
        self.flow_grid = np.zeros(self.data_length)
        self.flow_loss = np.zeros(self.data_length)
        self.now = 0
        self.now_state_battery = self.settings_battery.initial_soc
        self.now_balance = self.data_balance[self.now]
        
        self.old_fitness = 0, 0
        
        if reset_attributes:
            self.routine_attr = {}
            self.routine_attr_id = 0
            self.routine_attr_bool = True
    
    def validate_flows(self):
        total_value = np.sum(self.data_load + self.data_pv + self.flow_battery + self.flow_grid + self.flow_loss)
        try:
            assert total_value == 0. 
        except AssertionError:
            print("ASSERT_ERR:\t",total_value)
            
    @property
    def costs(self):
        to_grid = self.flow_grid >= 0
        profit = np.sum(self.data_sell_prices[to_grid] * self.flow_grid[to_grid])
        # add - to costs, because data flow from grid is negative
        cost = -np.sum(self.data_buy_prices[~to_grid] * self.flow_grid[~to_grid]) 
        return cost - profit
    
    @property
    def green(self):
        total_pv = -np.sum(self.data_pv)  # add - because, data flow is negative from pv
        total_loss = np.sum(self.flow_loss)
        total_to_grid = np.sum(self.flow_grid[self.flow_grid >= 0])
        total_load = np.sum(self.data_load)
        
        # this could be > 1 if the state of the battery
        # is better at the end then at the beginning
        return (total_pv - total_loss - total_to_grid) / total_load
    
    @property
    def not_green(self):
        return 100 * (1. - self.green)
    
    @property
    def fitness(self):
        return self.costs, self.not_green
    
    @property
    def max_peak(self):
        return -self.flow_grid.min()
    
    @property
    def battery_context_switches(self):
        # is battery charging or discharging
        signed = np.sign(self.flow_battery)
        
        # contracts the state where battery is not used
        # TODO: when it's no used is probably discharging with the self discharge rate
        signed_non_zero = signed[signed != 0]
        
        # when context switches the sign of two consecutive
        # readings will be different, so -1
        switches = signed_non_zero[1:] * signed_non_zero[:-1]
        
        # we remove 1 to set the same contexts to 0 and use numpy function to count others
        return np.count_nonzero(switches - 1)
    
    @property
    def all_objectives(self):
        return self.costs, self.not_green, self.max_peak, self.battery_context_switches
    
    def export_routine_attr(self):
        return deepcopy(self.routine_attr)
    
    def get_routine_attr(self):
        """
        If the value for the feature node currently in use
        already exist then return it, else generate a new 
        value for this node.
        """
        try:
            return self.routine_attr[self.routine_attr_id]
        except:
            val = random()
            self.routine_attr[self.routine_attr_id] = val
            return val
            
    def set_routine_attr(self, child):
        """
        Generate an id for the next feature node to be checked.
        """
        if child:
            self.routine_attr_id = self.routine_attr_id * 2 + 1
        else:
            self.routine_attr_id = self.routine_attr_id * 2 + 2
        
        self.routine_attr_bool = child
            
    def check_feature_leq(self, feature_name):
        '''
        This method check whether the feature called `feature_name`
        is less then the dynamically changed `value` with respect to the
        min and max changing feature values.
        '''
        # get the value to which the current value coresponding to
        # the feature_name will be compared
        value = self.get_routine_attr()
        
        # the value is not absolute, it changes as the decision tree
        # is traversed, therefore it needs to be computed
        feature_min = self.features_dynamic[feature_name + '_min']
        feature_max = self.features_dynamic[feature_name + '_max']
        check_value = feature_min + value * (feature_max - feature_min)
        
        if feature_name.startswith('pday_avg_'):
            #
            now_value = getattr(self, feature_name.replace('pday_avg_',''))[self.now]
        else:
            #
            now_value = getattr(self, feature_name)[self.now]
        # just a commment
        # for now all the features are precomputed, this could change in the future
            
        # need to change the boundaries in which the feature operates
        if now_value <= check_value:
            self.features_dynamic[feature_name + '_max'] = check_value
        else:
            self.features_dynamic[feature_name + '_min'] = check_value
        
        self.set_routine_attr(now_value <= check_value)
        return now_value <= check_value
    
    ##################################################################
    ##################################################################
    ##################################################################
    #                     FEATURES - COMPUTED
    
    def if_minute_of_day_leq(self, out1, out2):
        return partial(if_then_else, partial(self.check_feature_leq, 'minute_of_day'), out1, out2)
    
    def if_pday_avg_data_buy_prices_leq(self, out1, out2):
        return partial(if_then_else, partial(self.check_feature_leq, 'pday_avg_data_buy_prices'), out1, out2)
    
    def if_pday_avg_data_sell_prices_leq(self, out1, out2):
        return partial(if_then_else, partial(self.check_feature_leq, 'pday_avg_data_sell_prices'), out1, out2)
    
    def if_diff_pday_avg_data_buy_prices_leq(self, out1, out2):
        return partial(if_then_else, partial(self.check_feature_leq, 'diff_pday_avg_data_buy_prices'), out1, out2)
    
    def if_diff_pday_avg_data_sell_prices_leq(self, out1, out2):
        return partial(if_then_else, partial(self.check_feature_leq, 'diff_pday_avg_data_sell_prices'), out1, out2)
    
    ##################################################################
    #    FEATURES to TERMINAL
    
    def if_minute_of_day_leq_then_battery(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'minute_of_day'),
                       self.use_battery_first, self.use_grid_first)()
    
    def if_pday_avg_data_buy_prices_leq_then_battery(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'pday_avg_data_buy_prices'), 
                       self.use_battery_first, self.use_grid_first)()
    
    def if_pday_avg_data_sell_prices_leq_then_battery(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'pday_avg_data_sell_prices'), 
                       self.use_battery_first, self.use_grid_first)()
    
    def if_diff_pday_avg_data_buy_prices_leq_then_battery(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'diff_pday_avg_data_buy_prices'), 
                       self.use_battery_first, self.use_grid_first)()
    
    def if_diff_pday_avg_data_sell_prices_leq_then_battery(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'diff_pday_avg_data_sell_prices'), 
                       self.use_battery_first, self.use_grid_first)()
    
    # -------------------------------------------------------
    def if_minute_of_day_leq_then_grid(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'minute_of_day'),
                       self.use_grid_first, self.use_battery_first)()
    
    def if_pday_avg_data_buy_prices_leq_then_grid(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'pday_avg_data_buy_prices'), 
                       self.use_grid_first, self.use_battery_first)()
    
    def if_pday_avg_data_sell_prices_leq_then_grid(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'pday_avg_data_sell_prices'), 
                       self.use_grid_first, self.use_battery_first)()
    
    def if_diff_pday_avg_data_buy_prices_leq_then_grid(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'diff_pday_avg_data_buy_prices'), 
                       self.use_grid_first, self.use_battery_first)()
    
    def if_diff_pday_avg_data_sell_prices_leq_then_grid(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'diff_pday_avg_data_sell_prices'), 
                       self.use_grid_first, self.use_battery_first)()
    
        
    ##################################################################
    ##################################################################
    #              FEATURES - loaded
    
    def if_load_leq(self, out1, out2):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_load'), out1, out2)
        
    def if_pv_leq(self, out1, out2):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_pv'), out1, out2)
    
    def if_buy_prices_leq(self, out1, out2):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_buy_prices'), out1, out2)
    
    def if_sell_prices_leq(self, out1, out2):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_sell_prices'), out1, out2)
    
    ##################################################################
    #           FEATURES to TERMINALS
    
        
    def if_load_leq_then_battery(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_load'),
                       self.use_battery_first, self.use_grid_first)()
        
    def if_pv_leq_then_battery(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_pv'), 
                       self.use_battery_first, self.use_grid_first)()
    
    def if_buy_prices_leq_then_battery(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_buy_prices'), 
                       self.use_battery_first, self.use_grid_first)()
    
    def if_sell_prices_leq_then_battery(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_sell_prices'), 
                       self.use_battery_first, self.use_grid_first)()
    #-----------------------------------------------------------------
        
    def if_load_leq_then_grid(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_load'), 
                       self.use_grid_first, self.use_battery_first)()
        
    def if_pv_leq_then_grid(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_pv'),  
                       self.use_grid_first, self.use_battery_first)()
    
    def if_buy_prices_leq_then_grid(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_buy_prices'),  
                       self.use_grid_first, self.use_battery_first)()
    
    def if_sell_prices_leq_then_grid(self):
        return partial(if_then_else, partial(self.check_feature_leq, 'data_sell_prices'),  
                       self.use_grid_first, self.use_battery_first)()
    
    ##################################################################
    #                SIMULATION
    ##################################################################
    
    def _make_step(self, routine, infinite=False):
        #if self.now >= self.data_length:
        #    if infinite:
        #        self._reset()
        #    else:
        #        return False
        self.features_dynamic.update(self.features_static)
        self.routine_attr_id = 0
        #new_flow_battery, new_flow_grid = routine()
        if self.SAR_tracking:
            sar_trace_state = tuple(self.state())
            sar_trace_old_objectives = tuple(self.all_objectives)
            
        routine()
        
        if self.SAR_tracking:
            sar_trace_action = self.action_executed
        
        new_flow_battery, new_flow_grid = self.new_flow_battery, self.new_flow_grid

        self.flow_battery[self.now] = new_flow_battery
        self.flow_grid[self.now] = new_flow_grid

        new_flow_loss = self.now_balance - new_flow_battery - new_flow_grid
        self.flow_loss[self.now] = new_flow_loss

        self.state_battery[self.now] = self.now_state_battery + new_flow_battery
        
        if self.debug:
            assert self.state_battery[self.now] >= self.settings_battery.min_soc

        if self.debug:
            calculated_flow = self.flow_battery[self.now] + self.flow_grid[self.now] + self.flow_loss[self.now]
            data_flow = self.data_load[self.now] + self.data_pv[self.now]
            try:
                assert abs(calculated_flow + data_flow) < 1e-10
            except:
                print(calculated_flow, data_flow, calculated_flow + data_flow)
                raise AssertionError

        self.now += 1
        continue_simulation = True
        if self.now < self.data_length:
            self.now_state_battery = self.state_battery[self.now-1]
            self.now_balance = self.data_balance[self.now]
        else:
            # we have reached the end of data
            # should we continue simulation from 
            # the begining or end it?
            if infinite:
                self._reset(reset_attributes=False)
            else:
                continue_simulation = False
        
        if self.SAR_tracking:
            sar_trace_new_objectives = self.all_objectives
            sar_trace_reward = tuple([-(n - o) for o,n in zip(sar_trace_old_objectives, sar_trace_new_objectives)])

            self.state_action_reward_trace.append((sar_trace_state,
                                                   sar_trace_action,
                                                   sar_trace_reward))
        
        return continue_simulation
    
    def run(self, routine, reset_attributes=True, run_for=None):
        if run_for is None:
            run_for = np.inf
        
        stop = self.now + run_for
        
        self._reset(reset_attributes)
        # run until your next step gets you
        # to the last state in the data, so -1
        #while self.now < self.data_length - 1:
        #    self._make_step(routine)
        step_made = True
        while step_made and self.now + 1 <= stop:
            step_made = self._make_step(routine)
            
    def run_timetable(self, action_ids, reset_attributes=True, run_for=None):
        
        """
        Timetable is a list of
        action ids: [action_id_1, action_id_2, ...]
        for every possible minute_of_the_day value.
        """
        if run_for is None:
            run_for = np.inf
        
        stop = self.now + run_for
        
        # reset the values at the attributs
        self._reset(reset_attributes)
        
        # get the possible actions
        actions = self.get_actions()
        # generate a mapping minute_of_day -> action_id
        minutes_diff = self.minute_of_day[1] - self.minute_of_day[0]
        steps = int(24 * 60 // minutes_diff)
        #print(steps, type(steps))
        if len(action_ids) == steps:
            daily = True
            action_to_make = {x:action_ids[i] for i, x in enumerate(self.minute_of_day[:steps])}
        else:
            daily = False
            if len(action_ids) != len(self.data_time):
                print("Unable to use the timetable of len {} != {}, !={}".format(len(action_ids), steps, len(self.data_time)))
            action_to_make = cycle(action_ids)
        
        # run simulator
        step_made = True
        while step_made and self.now <= stop:
            if daily:
                action_id = action_to_make[self.minute_of_day[self.now]]
            else:
                action_id = next(action_to_make)
            step_made = self._make_step(actions[action_id])
        
    def run_model(self, model, reset_attributes=True, run_for=None):
        
        """
        Model is a model, that given the features, compute the
        action.
        """
        if run_for is None:
            run_for = np.inf
        
        stop = self.now + run_for
        
        # reset the values at the attributs
        self._reset(reset_attributes)
        
        # get the possible actions
        actions = self.get_actions()
        
        # run simulator
        step_made = True
        while step_made and self.now <= stop:
            action_id = model.predict(self.state())
            step_made = self._make_step(actions[action_id])
        
        
    def frame_step(self, routine, infinite=False):
        self.old_fitness = self.fitness
        
        step_made = self._make_step(routine, infinite)
        terminal = not step_made
        reward = tuple([-(n - o) for o,n in zip(self.old_fitness, self.fitness)])
        try:
            state = self.state()
        except IndexError:
            terminal = True
            state = self.state(for_time=0)
        
        return state, reward, terminal
    
    
    def state(self, for_time=None):
        # data_time.dayofweek, data_time.month, data_time.hour, minute_of_day
        # data_load, data_pv, data_buy_prices, data_sell_prices
        # pday_avg_data_buy_prices, pday_avg_data_sell_prices
        if for_time is None:
            for_time = self.now
        state_ = [
            self.data_time.dayofweek[for_time],
            self.data_time.month[for_time],
            self.data_time.hour[for_time],
            self.minute_of_day[for_time],
            self.data_load[for_time],
            self.data_pv[for_time],
            self.data_buy_prices[for_time],
            self.data_sell_prices[for_time],
            self.pday_avg_data_buy_prices[for_time],
            self.pday_avg_data_sell_prices[for_time],
            self.diff_pday_avg_data_buy_prices[for_time],
            self.diff_pday_avg_data_sell_prices[for_time],
            self.now_state_battery,
        ]
        state_array = np.zeros(len(state_))
        for i,s in enumerate(state_):
            state_array[i] = s
        return state_array
            
    ##################################################################
    #                ROUTINES
    ##################################################################
    
    
    # ------------ BASIC ACTIONS ------------------------------------#
    
    def first_to_battery(self):
        """
        Direct the flow to battery first then to grid.
        """
        self.new_flow_battery = min(self.settings_battery.max_charge,
                               self.settings_battery.max_soc - self.now_state_battery,
                               self.now_balance)
        
        self.new_flow_grid = min(self.settings_grid.max_to_grid,
                            self.now_balance - self.new_flow_battery)
        
        if self.debug:
            assert self.new_flow_battery >= 0
            assert self.new_flow_grid >= 0
            assert self.new_flow_battery + self.new_flow_grid <= self.now_balance
            

    def first_to_grid(self):
        """
        Direct the flow to grid first, then to battery if the grid
        can't take it all.
        """
        self.new_flow_grid = min(self.settings_grid.max_to_grid,
                            self.now_balance)
        
        self.new_flow_battery = min(self.settings_battery.max_charge,
                               self.settings_battery.max_soc - self.now_state_battery,
                               self.now_balance - self.new_flow_grid)
        
        if self.debug:
            assert self.new_flow_battery >= 0
            assert self.new_flow_grid >= 0
            assert self.new_flow_battery + self.new_flow_grid <= self.now_balance

    
    def first_from_battery(self):
        """
        Direct the flow FROM battery first, then from grid if necessary.
        Balance should be negative
        """
        self.new_flow_battery = - min(self.settings_battery.max_discharge,
                                    self.now_state_battery - self.settings_battery.min_soc,
                                    -self.now_balance)
        self.new_flow_grid = self.now_balance - self.new_flow_battery
        
        if self.debug:
            assert self.now_balance <= 0
            assert self.new_flow_grid <= 0
            assert self.new_flow_battery <= 0
        
    
    def first_from_grid(self):
        """
        Direct the flow from grid first. There are no limits.
        """
        # since the grid can provide unlimited energy, none is needed from battery
        self.new_flow_battery = 0.
        
        # the balance is negative in this case, so should be the grid flow
        self.new_flow_grid = self.now_balance
        
        if self.debug:
            assert self.now_balance <= 0

    
    # ------------ COMPOSED ACTIONS --------------------------------#
    
    def default_routine(self):
        """
        This is a default controller that uses the battery first if there is lack
        of energy and grid first if there is energy surplus.
        """                   
        if self.now_balance >= 0:
            self.first_to_grid()
        else:
            self.first_from_battery()
            
        self.action_executed = "default_routine"
    
    def use_battery_first(self):
        """
        This routine uses battery first (in and out) and a grid as a backup.
        """
        if self.now_balance >= 0:
            self.first_to_battery()
        else:
            self.first_from_battery()
        
        self.action_executed = "use_battery_first"
        
    def use_grid_first(self):
        """
        This routine uses grid first (in and out) and a battery as a backup.
        """
        if self.now_balance >= 0:
            self.first_to_grid()
        else:
            self.first_from_grid()
            
        self.action_executed = "use_grid_first"
    
    def use_oportunist_sell(self):
        if self.now_balance <= 0:
            self.first_from_battery()
        else:
            if self.data_sell_prices[self.now] <= self.pday_avg_data_sell_prices[self.now]:
                self.first_to_battery()
            else:
                self.first_to_grid()
    
        self.action_executed = "use_oportunist_sell"
        
        
    def get_actions(self):
        actions = [
            self.use_battery_first,
            self.use_grid_first
        ]
        return actions
    
    def get_action_name_to_id(self, action_name):
        action_to_id = {
            "use_battery_first": 0,
            "use_grid_first": 1,
            "default_routine": 2,
            "use_oportunist_sell": 3,
        }
    
    # maybe something should be done about the terminals, now two sibling
    # terminals can be the same
    

def get_test_smart_house():
    ## how does the settings look like?
    settings_string = """
    {
    "devices":
        {
        "battery":
            {
            "max_charge": 12.5,
            "max_discharge": 12.5,
            "min_soc": 1.0,
            "max_soc": 50.0,
            "initial_soc": 1.0,
            "self_discharge_rate": 0.0,
            "charge_efficiency": 1.0,
            "discharge_efficiency": 1.0
            },
        "grid":
            {
            "max_to_grid": 7.0,
            "to_grid_efficiency": 1.0
            },
        "pv":
            {
            "peak_power": 10.0
            }
        }
    }
    """
    settings_test = json.loads(settings_string)
    battery_conf = Battery_conf(**settings_test.get('devices').get('battery'))
    grid_conf = Grid_conf(**settings_test.get('devices').get('grid'))
       
    from data import load_data
    data_in = load_data('2014-04-01', '2014-10-15',
                           'data/processed/consumption/ld2011_2014.csv', "MT_003",
                          'data/processed/prices/epexspotde.csv', 'weighted_avg', 'weighted_avg',
                          'data/processed/weather/1824.csv', 10)
    
    SHt = SmartHouse(data_in, battery_conf, grid_conf, features=None, debug=True)
    return SHt
    
if __name__ == "__main__":
    
    SHt = get_test_smart_house
    a1, a2 = SHt.get_actions()
    
    print("Data load (min, max)", SHt.data_load.min(), SHt.data_load.max())
    print("Data pv (min, max)", SHt.data_pv.min(), SHt.data_pv.max())
    print("Data balance (min, max)", SHt.data_balance.min(), SHt.data_balance.max())
    
    
    print('Battery first: ')
    SHt.run(SHt.use_battery_first)
    print(f'\t\t{SHt.fitness}\t{SHt.max_peak}\n\n')
    SHt.validate_flows()
    
    SHt = SmartHouse(data_in, battery_conf, grid_conf, features=None, debug=True)
    print('Grid first: ')
    SHt.run(SHt.use_grid_first)
    print(f'\t\t{SHt.fitness}\t{SHt.max_peak}\n\n')
    SHt.validate_flows()
    
    SHt = SmartHouse(data_in, battery_conf, grid_conf, features=None, debug=True)
    print('Oportunist: ')
    SHt.run(SHt.use_oportunist_sell)
    print(f'\t\t{SHt.fitness}\t{SHt.max_peak}')
    SHt.validate_flows()
    