import logging
import json
import uuid
import time
import random

import deap
from deap import creator, base, cma, algorithms, tools, gp
import operator
import numpy as np
from optimizers import optUtils
from tqdm import tqdm

def evaluate_routine(individual, attr_vals, smart_house, toolbox):
    routine = toolbox.compile(individual)
    reset_attributes = True
    if attr_vals is not None:
        keys = sorted(smart_house.routine_attr.keys())
        new_attrs = dict(zip(keys, attr_vals))
        smart_house.routine_attr.update(new_attrs)
        reset_attributes = False
    tic = time.time()
    smart_house.run(routine, reset_attributes)
    toc = time.time()
    individual.routine_attr = smart_house.export_routine_attr()
    individual.features_static = smart_house.features_static
    individual.max_peak = smart_house.max_peak
    individual.uuid = str(uuid.uuid4())
    individual.eval_duration = f"{toc - tic}"
    individual.battery_context_switches = smart_house.battery_context_switches
    individual.sar_trace = smart_house.state_action_reward_trace
    return smart_house.fitness[0], smart_house.fitness[1]


def selTournamentDcd(individuals, k):
            assert len(individuals) % 4 == 0
            tools.emo.assignCrowdingDist(individuals)
            return tools.selTournamentDCD(individuals, k)
        
        
class GPOptimizer(object):
    def __init__(self, smart_house,
                 NMU, NLAMBDA, NGEN, MUTPB, CXPB,
                 INIT_MIN_DEPTH, INIT_MAX_DEPTH,
                 MUT_MIN_DEPTH, MUT_MAX_DEPTH,
                 MAX_DEPTH,
                 logdir):
        
        self.individualsPopLog = logdir + "/logIndividualsPopulation.log"
        self.individualsOffLog = logdir + "/logIndividualsOffspring.log"
        self.populationLog = logdir + "/logPopulation.log"
        
        self.pset = gp.PrimitiveSet("MAIN", 0)
        
        tests = [s for s in dir(smart_house) if s.startswith('if') and s.endswith('leq')]
        for test in tests:
            self.pset.addPrimitive(getattr(smart_house, test), 2)
        
        terminals = [s for s in dir(smart_house) if s.startswith('if') and (s.endswith('battery') or s.endswith('grid'))]
        for term in terminals:
            self.pset.addTerminal(getattr(smart_house, term))
        
        self.NMU = NMU
        self.NLAMBDA = NLAMBDA
        self.NGEN = NGEN
        self.MUTPB = MUTPB
        self.CXPB = CXPB
        self.INIT_MIN_DEPTH = INIT_MIN_DEPTH
        self.INIT_MAX_DEPTH = INIT_MAX_DEPTH
        self.MUT_MIN_DEPTH = MUT_MIN_DEPTH
        self.MUT_MAX_DEPTH = MUT_MAX_DEPTH
        self.MAX_DEPTH = MAX_DEPTH

        creator.create("FitnessMin", base.Fitness, weights=(-1.0,-1.0))
        creator.create("Individual", gp.PrimitiveTree, fitness=creator.FitnessMin)

        self.toolbox = base.Toolbox()
        self.toolbox.register("expr",
                              gp.genHalfAndHalf,
                              pset=self.pset,
                              min_=self.INIT_MIN_DEPTH,
                              max_=self.INIT_MAX_DEPTH)
        
        self.toolbox.register("individual",
                              tools.initIterate,
                              creator.Individual,
                              self.toolbox.expr)
        
        self.toolbox.register("population",
                              tools.initRepeat,
                              list,
                              self.toolbox.individual)
        
        self.toolbox.register("compile",
                              gp.compile,
                              pset=self.pset)
        
        self.toolbox.register("map", map)
        
        self.toolbox.register("evaluate",
                              evaluate_routine,
                              smart_house=smart_house,
                              attr_vals=None,
                              toolbox=self.toolbox)
        
        #self.toolbox.register("select", selTournamentDcd)
        self.toolbox.register("select", tools.selNSGA2)
        
        self.toolbox.register("mate", gp.cxOnePoint)
        
        self.toolbox.register("expr_mut",
                              gp.genHalfAndHalf,
                              min_=self.MUT_MIN_DEPTH,
                              max_=self.MUT_MAX_DEPTH)
        
        self.toolbox.register("mutate",
                              gp.mutUniform,
                              expr=self.toolbox.expr_mut,
                              pset=self.pset)

        self.toolbox.decorate("mate",
                              gp.staticLimit(key=operator.attrgetter("height"),
                                             max_value=self.MAX_DEPTH))
        self.toolbox.decorate("mutate", 
                              gp.staticLimit(key=operator.attrgetter("height"),
                                             max_value=self.MAX_DEPTH))


        self.stats = tools.Statistics(lambda ind: ind.fitness.values)
        self.stats.register("avg", np.mean, axis=0)
        self.stats.register("std", np.std, axis=0)
        self.stats.register("min", np.min, axis=0)
        self.stats.register("max", np.max, axis=0)
        
        self.logbook = tools.Logbook()
        self.logbook.header = ['time', 'gen', 'nevals'] + self.stats.fields
                
    def optimize(self):
        toolbox = self.toolbox
        logbook = self.logbook
        
        population = toolbox.population(n=self.NMU)
        
        invalid_ind = [ind for ind in population if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        record = self.stats.compile(population)
        logbook.record(time=str(time.time()), gen=0, nevals=len(invalid_ind), **record)
        optUtils.population_logbook_log(logbook, self.populationLog)
        
        optUtils.log_individuals(population, self.individualsPopLog, 0)
        
        for g in tqdm(range(1, self.NGEN+1), desc="Optimization GP"):
            random.shuffle(population)
            offspring = algorithms.varOr(population, toolbox, self.NLAMBDA, self.CXPB, self.MUTPB)
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit
            
            optUtils.log_individuals(offspring, self.individualsOffLog, g)
            
            population = toolbox.select(population + offspring, self.NMU)
            
            record = self.stats.compile(population)
            logbook.record(time=str(time.time()), gen=g, nevals=len(invalid_ind), **record)
            optUtils.population_logbook_log(logbook, self.populationLog)
            
            optUtils.log_individuals(population, self.individualsPopLog, g)
        
        return population, None
