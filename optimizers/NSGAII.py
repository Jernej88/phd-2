import logging
import json
import random
import uuid
import time
import operator

import deap
from deap import creator, base, cma, algorithms, tools, gp
import numpy as np
from tqdm import tqdm

from optimizers import optUtils


def evaluate_timetable(individual, smart_house):
    tic = time.time()
    smart_house.run_timetable(individual)
    toc = time.time()
    individual.max_peak = smart_house.max_peak
    individual.eval_duration = f"{toc - tic}"
    individual.uuid = str(uuid.uuid4())
    individual.battery_context_switches = smart_house.battery_context_switches
    individual.sar_trace = smart_house.state_action_reward_trace
    return smart_house.fitness[0], smart_house.fitness[1]
    
class GAOptimizer(object):
    def __init__(self, smart_house,
                 NMU, NLAMBDA, NGEN, MUTPB, CXPB,
                 logdir, optimal=False):
        
        self.individualsPopLog = logdir + "/logIndividualsPopulation.db"
        self.individualsOffLog = logdir + "/logIndividualsOffspring.db"
        self.populationLog = logdir + "/logPopulation.log"
                
        self.NMU = NMU
        self.NLAMBDA = NLAMBDA
        self.NGEN = NGEN
        self.MUTPB = MUTPB
        self.CXPB = CXPB

        creator.create("FitnessMin", base.Fitness, weights=(-1.0,-1.0))
        creator.create("Individual", list, fitness=creator.FitnessMin)

        self.toolbox = base.Toolbox()
        
        actions_length = len(smart_house.get_actions())
        if optimal:
            ind_length = len(smart_house.minute_of_day)
        else:
            minutes_diff = smart_house.minute_of_day[1] - smart_house.minute_of_day[0]
            ind_length = int(24 * 60 // minutes_diff)
        self.optimal = optimal
        
        self.toolbox.register("action_id", random.randint, a=0, b=actions_length-1)
        self.toolbox.register("individual",
                              tools.initRepeat,
                              creator.Individual,
                              self.toolbox.action_id,
                              n=ind_length)
        
        self.toolbox.register("population",
                              tools.initRepeat,
                              list,
                              self.toolbox.individual)
        
        self.toolbox.register("map", map)
        
        self.toolbox.register("evaluate",
                              evaluate_timetable,
                              smart_house=smart_house)
        
        self.toolbox.register("mate", tools.cxTwoPoint)
        self.toolbox.register("mutate", tools.mutFlipBit, indpb=1./ind_length)
        self.toolbox.register("select", tools.selNSGA2)

        self.stats = tools.Statistics(lambda ind: ind.fitness.values)
        self.stats.register("avg", np.mean, axis=0)
        self.stats.register("std", np.std, axis=0)
        self.stats.register("min", np.min, axis=0)
        self.stats.register("max", np.max, axis=0)
        
        self.logbook = tools.Logbook()
        self.logbook.header = ['time', 'gen', 'nevals'] + self.stats.fields
        
        
    def optimize(self):
        toolbox = self.toolbox
        logbook = self.logbook
        
        population = toolbox.population(n=self.NMU)
        
        invalid_ind = [ind for ind in population if not ind.fitness.valid]
        fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
        for ind, fit in zip(invalid_ind, fitnesses):
            ind.fitness.values = fit

        record = self.stats.compile(population)
        logbook.record(time=str(time.time()), gen=0, nevals=len(invalid_ind), **record)
        optUtils.population_logbook_log(logbook, self.populationLog)
        
        optUtils.log_individuals(population, self.individualsPopLog, 0)
        
        for g in tqdm(range(1, self.NGEN+1), desc="Optimization GA"):
            random.shuffle(population)
            offspring = algorithms.varAnd(population, toolbox, self.CXPB, self.MUTPB)
            invalid_ind = [ind for ind in offspring if not ind.fitness.valid]
            fitnesses = toolbox.map(toolbox.evaluate, invalid_ind)
            for ind, fit in zip(invalid_ind, fitnesses):
                ind.fitness.values = fit
            
            optUtils.log_individuals(offspring, self.individualsOffLog, g)
            
            population = toolbox.select(population + offspring, self.NMU)
            
            record = self.stats.compile(population)
            logbook.record(time=str(time.time()), gen=g, nevals=len(invalid_ind), **record)
            optUtils.population_logbook_log(logbook, self.populationLog)
            
            optUtils.log_individuals(population, self.individualsPopLog, g)
        
        return population, None
