import json
import dataset

def log_individuals(group, filename, generation, trace=False):
    return log_individuals_fs(group, filename, generation, trace)

def log_individuals_db(group, filename, generation, trace=False):
    db = dataset.connect('sqlite:///{}'.format(filename))
    table = db["individuals"]
    
    all_inds = []
    for ind in group:
        routine = ind.__str__()
        try:
            routine_attr = ind.routine_attr
            features_static = ind.features_static
        except AttributeError:
            routine_attr = {}
            features_static = {}
            pass
        fitness = ind.fitness.values
        max_peak = ind.max_peak
        uuid = ind.uuid
        eval_duration = ind.eval_duration
        battery_context_switches = ind.battery_context_switches
        if trace:
            sar_trace = ind.sar_trace
        else:
            sar_trace = []
        ind_stats = dict(
                        gen=generation,
                        routine=routine,
                        routine_attr=json.dumps(routine_attr),
                        static_features=json.dumps(features_static),
                        fitness=json.dumps(fitness),
                        max_peak=max_peak,
                        uuid=uuid,
                        eval_duration=eval_duration,
                        battery_context_switches=battery_context_switches,
                        sar_trace=json.dumps(sar_trace),
                        )
        all_inds.append(ind_stats)
    table.insert_many(all_inds)
        
            

def log_individuals_fs(group, filename, generation, trace=False):
    with open(filename, 'a') as fp:
        for ind in group:
            routine = ind.__str__()
            try:
                routine_attr = ind.routine_attr
                features_static = ind.features_static
            except AttributeError:
                routine_attr = {}
                features_static = {}
                pass
            fitness = ind.fitness.values
            max_peak = ind.max_peak
            uuid = ind.uuid
            eval_duration = ind.eval_duration
            battery_context_switches = ind.battery_context_switches
            
            if trace:
                sar_trace = ind.sar_trace
            else:
                sar_trace = []
            
            ind_stats = dict(
                            gen=generation,
                            routine=routine,
                            routine_attr=routine_attr,
                            static_features=features_static,
                            fitness=fitness,
                            max_peak=max_peak,
                            uuid=uuid,
                            eval_duration=eval_duration,
                            battery_context_switches=battery_context_switches,
                            sar_trace=sar_trace,
                        )
            fp.write(f"{json.dumps(ind_stats)}\n")
            
             
def population_logbook_log(logbook, filepath):
    with open(filepath, 'a') as fp:
            fp.write(f"{logbook.stream}\n")