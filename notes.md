# Notes

1. I could not find the weather data that would align with the forecast that we have. Therefore, we will omit the forecast data.
2. Load/consumption data varies by the order of magnitudes. For each consumer/client customized settings will be required (battery, pv size).
3. load data is already transformed to kWh (normal form) with the `process_consumption.py`
4. weather data is processed with `proces_weather.py` 
5. some data is missing, in the simulations it is filled with first previous value, this should be ok if there is missing data for an hour or two, however, when there are days or weeks of missing data, this could distort the results
