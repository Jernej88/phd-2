import numpy as np
import matplotlib.pyplot as plt
from experiment import Experiment

exp = Experiment()

pop, stats = exp.run()

train_fits, test_fits, *_ = exp.test(pop)
diffs = [(ftr[0] - fte[0], ftr[1] - fte[1]) for ftr, fte in zip(train_fits, test_fits)]    
diffs_cost, diffs_green = zip(*diffs)

print(f"Cost: {np.mean(diffs_cost)} [{np.std(diffs_cost)}]")
print(f"Green: {np.mean(diffs_green)} [{np.std(diffs_green)}]")

train_cost, train_green = zip(*train_fits)
test_cost, test_green = zip(*test_fits)

plt.plot(train_cost, train_green, 'o', label='Train')
plt.plot(test_cost, test_green, 'o', label='Test')
plt.legend()
plt.savefig('test.png')