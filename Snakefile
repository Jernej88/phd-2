def get_weather_station_ids():
    fp = 'data/weather_stations_id.csv'
    ids = []
    with open(fp, 'r') as ff:
        for l in ff:
            if l.startswith('#') or l.startswith('id'):
                continue
            try:
                id = int(l.split(',')[0])
                ids.append(id)

            except Exception as e:
                print(e)

    return ids

WEATHER_STATION_IDS = get_weather_station_ids()



##################################################################
#
##################################################################
rule all:
    input:
        "report.csv"

##################################################################
#                       GP OPTIMIZE
##################################################################
rule report:
    input:
        expand("results_gp/{ids}.csv", ids=WEATHER_STATION_IDS)
    output:
        "report.csv"
    shell:
        "echo 'REPORT STEP'"


rule optimize:
    input:
        expand("data/aligned/{id}.csv", id=WEATHER_STATION_IDS)
    output:
        expand("results_gp/{id}.csv", id=WEATHER_STATION_IDS)
    shell:
        "echo 'OPTIMIZATION STEP'"


##################################################################
#                       ALIGN DATA
##################################################################
rule align_data:
    input:
        "data/processed/consumption/ld2011_2014.csv",
        "data/processed/prices/epexspotde.csv",
        expand("data/processed/weather/{id}.csv", id=WEATHER_STATION_IDS)
    output:
        expand("data/aligned/{id}.csv", id=WEATHER_STATION_IDS)
    shell:
        "echo 'ALIGNMENT STEP'"


###################################################################
###################################################################
###################################################################
#
#                           IMPORTANT
#
# we assume the following is done already
# meaning we will distribute processed data
# because one never knows how the APIs will
# change in the future
#
# 
###################################################################
##                       PREPROCESS DATA
###################################################################
#rule process_consumption:
#    input:
#        "data/raw/consumption/LD2011_2014.txt"
#    output:
#        "data/processed/consumption/ld2011_2014.csv"
#    shell:
#        "python process_consumption.py"
#
#
#rule process_prices:
#    input:
#        "data/raw/prices/EPEXSPOTDE/"
#    output:
#        "data/processed/prices/epexspotde.csv"
#    shell:
#        "python process_prices.py"
#
#
#rule process_weather:
#    input:
#        expand("data/raw/weather/{id}.csv", id=WEATHER_STATION_IDS)
#    output:
#        expand("data/processed/weather/{id}.csv", id=WEATHER_STATION_IDS)
#    shell:
#        "python process_weather.py"
#    
#
###################################################################
##                       GET THE DATA
###################################################################
#rule get_raw_consumption:
#    output:
#        "data/raw/consumption/LD2011_2014.txt",
#        "data/raw/consumption/household_power_consumption.txt"
#    shell:
#        "python get_raw_consumption.py"
#
#
#rule get_raw_prices:
#    output:
#        "data/raw/prices/EPEXSPOTDE/",
#        "data/raw/prices/PJM/"
#    shell:
#        "python get_raw_prices.py"
#
#
#rule get_raw_weather:
#    output:
#        expand("data/raw/weather/{id}.csv", id=WEATHER_STATION_IDS)
#    shell:
#        "python get_raw_weather.py"