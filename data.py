import pandas as pd
from collections import namedtuple

def load_data_consumption(path, start, end, cid):
    """
    cid is of shape MT_XXX where XXX goes from 001 to 370
    """
    consumption = pd.read_csv(path, index_col=0, parse_dates=[0])
    consumption = consumption.loc[start:end, [cid]]
    
    # the following shape will be expected 
    # datetime,value
    # 2014-06-01 00:00:00,0.0
    consumption.rename(columns={cid: "value"}, inplace=True)
    consumption.index.names = ["datetime"]
    
    # values are already in kWh
    return consumption.fillna(method="ffill")
    
    
def load_data_prices(path, start, end, buy="weighted_avg", sell="weighted_avg"):
    """
    buy/sell can be from ["low", "high", "last", "wegihted_avg"]
    """
    prices = pd.read_csv(path, index_col=0, parse_dates=[0])
    prices.index.names = ["datetime"]
    prices = prices.loc[start:end]
    
    return_prices = pd.DataFrame()
    return_prices["buy_price"] = prices[buy].copy()
    return_prices["sell_price"] = prices[sell].copy()
    return return_prices.fillna(method="ffill")
    

def add_production(data, max_power=1., wind=False):
    "Add PV production per squared meter to a weather data DF"

    # the regression model used based on
    # Photovoltaic Modules Operating Temperature Estimation Using a Simple Correlation
    # A. M. Muzathik, International Journal of Energy Engineering, Aug. 2014, Vol. 4 Iss. 4, PP. 151-158
    #
    # hitrost vetra - m/s (wind speed)
    # temperatura - C (temperature)
    # sevanje - W/m² (radiation)
    # globalno sev. (global radiation)
    # difuzno sev. (difuse radiation)
    # povp. T (average Temperature in the time window)
    
    k = -0.004
    standard_irradiance = 1000.0
    standard_temperature = 25.0
    if 'hitrost vetra' in data.columns and wind:
        data['production'] = (data['difuzno sev.']) / standard_irradiance * (
            1.0 + k * 
            (0.943 * data['povp. T'] + 
             0.0195 * (data['difuzno sev.']) - 
             1.528 * data['hitrost vetra'] + 
             0.3529 - standard_temperature))
    else:
        data['production'] = (data['difuzno sev.']) / standard_irradiance * (
            1.0 + k * (data['povp. T'] - standard_temperature))
    
    
    data = data.resample('30min').mean()
    data['production'] = max_power * data['production'] * 0.5# transform to kWh for 30min time window
    return data


def load_weather_data(path, start, end, max_power):
    weather_data = pd.read_csv(path, index_col=0, parse_dates=[0])
    weather_data.fillna(method='ffill', inplace=True)
    weather_data = weather_data.loc[start:end]
    
    weather_data = add_production(weather_data, max_power=max_power, wind=True)
    
    weather_data_return = pd.DataFrame()
    weather_data_return["value"] = weather_data["production"]
    weather_data_return.index.names = ["datetime"]
    return weather_data_return


Data_in = namedtuple('Data_in', 'data_time data_load data_pv data_buy_prices data_sell_prices')


def validate_Data_in(data_in):
    assert data_in.data_time.shape[0] == data_in.data_load.shape[0]
    assert data_in.data_load.shape[0] == data_in.data_pv.shape[0]
    assert data_in.data_pv.shape[0] == data_in.data_buy_prices.shape[0]
    assert data_in.data_buy_prices.shape[0] == data_in.data_sell_prices.shape[0]
    
def load_data(start, end,
              load_path, load_id, 
              prices_path, prices_buy_id, prices_sell_id,
              weather_path, weather_max_power):
    
    consumption_data = load_data_consumption(load_path,
                          start, end, load_id)["value"].values
    prices_data = load_data_prices(prices_path, start, end,
                                        prices_buy_id, prices_sell_id)
    buy_prices = prices_data["buy_price"].values
    sell_prices = prices_data["sell_price"].values
    production_data = load_weather_data(weather_path, start, end, weather_max_power)["value"].values
    
    time = prices_data.index.to_series().values
    time = pd.to_datetime(time)
    
    data = Data_in(data_time=time,
                   data_load=consumption_data,
                   data_pv=-production_data,
                   data_buy_prices=buy_prices,
                   data_sell_prices=sell_prices)
    
    validate_Data_in(data)
    
    return data


def load_train_test_data(train_start, train_end,
                         test_start, test_end,
                          load_path, load_id, 
                          prices_path, prices_buy_id, prices_sell_id,
                          weather_path, weather_max_power):
    
    train = load_data(train_start, train_end,
              load_path, load_id, 
              prices_path, prices_buy_id, prices_sell_id,
              weather_path, weather_max_power)
    
    test = load_data(test_start, test_end,
              load_path, load_id, 
              prices_path, prices_buy_id, prices_sell_id,
              weather_path, weather_max_power)
    
    return train, test
    
if __name__ == "__main__":
    #print(load_data_consumption('data/processed/consumption/ld2011_2014.csv',
    #                      '2014-06-01', '2014-06-30', "MT_002").tail().to_csv())
    
    #print(load_data_prices('data/processed/prices/epexspotde.csv', "2014-06-01", "2014-06-30").tail().to_csv())
    
    #print(load_weather_data("data/processed/weather/1824.csv", "2014-06-01", "2014-06-30", 5000).tail().to_csv())
    
    print(load_data('2014-06-01', '2014-06-02',
              'data/processed/consumption/ld2011_2014.csv', "MT_002",
              'data/processed/prices/epexspotde.csv', 'weighted_avg', 'weighted_avg',
              'data/processed/weather/1824.csv', 5000))
    
    
    print(load_train_test_data('2014-06-01', '2014-06-02',
                               '2014-06-03', '2014-06-04',
              'data/processed/consumption/ld2011_2014.csv', "MT_002",
              'data/processed/prices/epexspotde.csv', 'weighted_avg', 'weighted_avg',
              'data/processed/weather/1824.csv', 5000))
    
    
    
