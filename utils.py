"""Utility functions reside here."""

from functools import wraps
import pkg_resources
import sys
import pandas as pd


def copy_load(src, load_id, dest):
    #consumption = pd.read_csv(src, index_col=0, parse_dates=[0])
    #consumption[load_id].to_csv(dest, hea)
    pd.read_csv(src, index_col=0, parse_dates=[0]).to_csv(dest, columns=[load_id], header=True, index=True)
    
def write_runtime(fp):
    with open(fp, 'w') as ff:
        version_str = f"{sys.version_info.major}.{sys.version_info.minor}.{sys.version_info.micro}"
        ff.write(version_str)

def write_runtime_full(fp):
    with open(fp, 'w') as ff:
        ff.write(sys.version)

def write_env(fp):
    with open(fp, 'w') as ff:
        lines = [f"{d.project_name}=={d.version}\n" for d in pkg_resources.working_set]
        ff.writelines(lines)
    
    
def retry(n_retries):
    "Retry decorator that calls a function n_retries-times"
    def retry_decorator(func):
        @wraps(func)
        def func_wrapper(*args, **kwargs):
            for retry in range(n_retries):
                try:
                    func(*args, **kwargs)
                    return True
                except Exception as e:
                    print('[ERROR]: ', e)
                    f_name = func.__name__
                    f_args = '{}'.format(args)
                    f_kwargs = '{}'.format(kwargs)
                    if retry + 1 < n_retries:
                        print('[RETRY]: %s(*%s,**%s)' % (f_name,
                                                         f_args,
                                                         f_kwargs))
                    else:
                        print('[FAIL]: %s(*%s,**%s)' % (f_name,
                                                        f_args,
                                                        f_kwargs))
                    pass
            print(args)
            return False
        return func_wrapper
    return retry_decorator
