from distutils.core import setup
from Cython.Build import cythonize

setup(
    ext_modules = cythonize("SmartHouseCy.pyx")
)

# if you have SmartHouseCy.pyx file here, you can build it
# >>> python setup.py build_ext --inplace