#!/usr/bin/env bash

papermill results-analysis.ipynb output/results-analysis-timetable.ipynb -p results_glob_pattern "timetable-logs/*"

papermill results-analysis.ipynb output/results-analysis-gp.ipynb -p results_glob_pattern "gp-logs/logs/*"
